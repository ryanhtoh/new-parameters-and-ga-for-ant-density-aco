# New Parameters and GA for Ant-Density ACO

Curiosity, Deposition Decay, and GA for Ant-Density ACO in
Dynamic Environments

Abstract
Ant Colony Optimization (ACO) algorithms are a popular heuristic algorithm for
optimization based on the behavior of ants. This paper expands on the original ant-density
ACO algorithm by adding a new curiosity and deposition decay parameter, which is shown to
improve the performance in the tested dynamic environments. We also utilize a metaevolutionary Genetic Algorithm system on ACO parameters, exploring the effect of genetic
recombination on individual genes versus genetic cloning. Research is done in a dynamic
environment that mimics the real natural environment via a continuous hexagonal grid with
food and nest goals.
