#!/usr/bin/env python
# coding: utf-8
# Imported from Jupyter Notebook


# IMPORTS
import pickle
import csv
import numpy as np
import cupy as cp
import matplotlib.pyplot as plt
import random

from cupyx.time import repeat

from PIL import Image

import pygame
import pygame_gui
get_ipython().run_line_magic('load_ext', 'cython')


# Settings
map_type = "FORK"
NUM_ANTS = 30
FOOD_GOAL = 3000
TIME_LIMIT = 100*1000
BATCH_SIZE = 8
TOURNAMENT_SIZE = 2
global ACTIVE_CASTES; ACTIVE_CASTES = 1
MAX_GEN = 10
ENABLE_CURIOSITY = True


# INIT GLOBALS
pygame.init()
clock = pygame.time.Clock()
antImg = pygame.image.load('ant_16.png')

global NUM_DIRS; NUM_DIRS = 6
global CELLS; CELLS = 65
global TOP; TOP = 50
global CELL_PIX; CELL_PIX = 14
global RES; RES = CELLS*CELL_PIX
global WHITE; WHITE = (255, 255, 255)
global BLACK; BLACK = (0, 0, 0)

FAN = np.asarray(Image.open('maps/fan.png'), dtype=np.int32)
FORK = np.asarray(Image.open('maps/2pellet.png'), dtype=np.int32)
MAZE = np.asarray(Image.open('maps/maze_easy.png'), dtype=np.int32)

def get_map(map_type):
    if map_type == "FAN": editable_map = FAN.copy()
    elif map_type == "FORK": editable_map = FORK.copy()
    elif map_type == "MAZE": editable_map = MAZE.copy()
    return editable_map


if ENABLE_CURIOSITY: cur_str = "cur"
else: cur_str = "nocur"    
filename = "data/23R_" + map_type + "_" + cur_str + ".csv"
editable_map = get_map(map_type)


# Cython
get_ipython().run_cell_magic('cython', '-a', 'from cpython cimport array\nimport array\nimport numpy as np\nimport random\n\ncdef int[:,:,:] dirs = np.array( [[[1,0],[0,1],[-1,1],[-1,0],[-1,-1],[0,-1]], \n                                                  [[1,0],[1,1],[0,1],[-1,0],[0,-1],[1,-1]]], dtype=np.int32 )\n\ncdef enum:\n    CELLS = 65\n    NUM_DIRS = 6\n    \n\ncdef class Ant:\n    cdef float max_dep, dep_decay, curiosity, elicitation, dep\n    cdef public int x, y, rot, food\n    cdef int curiosity_enabled\n    \n    def __init__(self, float[:] genes, ENABLE_CURIOSITY):\n        # Genetic variables\n        self.max_dep = genes[1]\n        self.dep_decay = genes[2]\n        self.curiosity = genes[3]\n        self.elicitation = genes[4]\n        self.curiosity_enabled = ENABLE_CURIOSITY\n        \n        # State variables\n        self.dep = float(self.max_dep)\n        self.x = int(CELLS/2)\n        self.y = int(CELLS/2)\n        self.rot = random.randint(0, 5)\n        self.food = 0\n        \n    def move(self, float[:,:,:,:] trail, int[:,:,:] maps, int col_food, int col_stored_food):\n        cdef int i, fed, hungry\n        cdef int next_x, next_y\n        cdef float rand, probs_sum, trail_value\n        #cdef float[:] probs\n        \n        if maps[self.x, self.y, 1]: # At food\n            if self.food == 0:\n                maps[self.x, self.y, 1] -= 1\n                self.food = 1\n                \n            self.dep = float(self.max_dep)\n            self.rot -= 3\n            if self.rot < 0: self.rot += 6\n        elif maps[self.x, self.y, 0]: # At home\n            if self.food == 1:\n                self.food = 0\n                col_food += 1\n                col_stored_food += 1\n                \n            self.dep = float(self.max_dep)\n            self.rot -= 3\n            if self.rot < 0: self.rot += 6\n        fed = self.food\n        hungry = 1\n        if fed: hungry = 0\n        probs = np.zeros(NUM_DIRS)\n        probs_sum = 0\n        stuck = False\n        for i in range(NUM_DIRS):\n            rot_diff = abs(self.rot - i)\n            if (rot_diff <= 1) or (rot_diff == 5): # Facing direction\n                next_x = self.x + dirs[self.y % 2, i, 0]\n                next_y = self.y + dirs[self.y % 2, i, 1]\n                if 0 < next_x < CELLS and 0 < next_y < CELLS: # Within bounds\n                    if maps[next_x, next_y, 2] == 0: # No wall\n                        trail_value = trail[next_x, next_y, i, fed]\n                        probs[i] = self.curiosity*self.curiosity_enabled + trail_value ** self.elicitation\n                        probs_sum += probs[i]\n            if i == 5 and probs_sum == 0:\n                stuck = True\n        if probs_sum > 0: probs /= probs_sum\n        probs_sum = 0\n        rand = random.random()\n        if not stuck:\n            self.rot = 0\n            for i in range(NUM_DIRS):\n                probs_sum += probs[i]\n                if rand < probs_sum:\n                    self.x += dirs[self.y % 2, i, 0]\n                    self.y += dirs[self.y % 2, i, 1]\n                    self.dep *= (1-self.dep_decay/1000)\n                    if self.dep < 0: self.dep = 0\n                    trail[self.x, self.y, i-3, hungry] += self.dep\n                    return i, col_food, col_stored_food\n                self.rot += 1\n            return self.rot, col_food, col_stored_food\n        else:\n            self.rot = random.randint(0, 5)\n            return self.rot, col_food, col_stored_food')


# Colony Class
class Colony():
    def __init__(self, id_num):
        self.id_num = id_num
        self.genes = []
        for caste in range(2):
            new_genes = np.array([random.uniform(0, 1), # evap
                                  random.uniform(0, 30),  # max dep
                                  random.uniform(0, 0), # dep decay
                                  random.uniform(0, 5), # curiosity
                                  random.uniform(0.9, 1.0) ], # elicit
                                  dtype=np.float32) 
            self.genes.append(new_genes)
        self.elapsed = 0
        self.food = 0
        self.stored_food = 0
        
    def print_info(self):
        for caste in range(ACTIVE_CASTES):
            if caste == 0:
                prepend = "ID: " + str(self.id_num) + ". Evap: "
            else:
                prepend = "         TR: "
            tgt = self.genes[caste]
            print("{:s}{:.3f}, Max Dep: {:.3f}, Dep Decay: {:.3f}, Curiosity: {:.3f}, A: {:.3f}"                  .format(prepend, tgt[0], tgt[1], tgt[2], tgt[3], tgt[4]) )
        
    def gene_recombination(self, mom, dad):
        for caste in range(2):
            for idx in range(len(self.genes)):
                rand = random.random()
                if rand < 0.5: self.genes[caste][idx] = mom.genes[caste][idx]
                else: self.genes[caste][idx] = dad.genes[caste][idx]
        self.elapsed = 0
        self.food = 0
        
    def caste_recombination(self, mom, dad):
        for caste in range(2):
            rand = random.random()
            if rand < 0.5: self.genes[caste][:] = mom.genes[caste][:]
            else: self.genes[caste][:] = dad.genes[caste][:]
        self.elapsed = 0
        self.food = 0
        
    def mutate(self):
        for caste in range(2):
            self.genes[caste] *= np.random.normal(1, [0.125, 0.125, 0.125, 0.125, 0.05], self.genes[caste].shape)


# In[6]:


def map_row(display, row_colors, row, colorkey):
    offset = 0
    if row % 2: offset = int(CELL_PIX/2)
    map_surface = pygame.surfarray.make_surface(row_colors)
    map_surface = pygame.transform.scale(map_surface, (RES, CELL_PIX))
    map_surface.set_colorkey(colorkey)
    display.blit(map_surface, (offset, row*CELL_PIX+TOP))

def reset_ants(colony, editable_map):
    editable_map = get_map(map_type).copy()
    if ENABLE_CURIOSITY:
        trail = np.zeros((CELLS, CELLS, NUM_DIRS, 2), dtype=np.float32)
    else:
        trail = np.full((CELLS, CELLS, NUM_DIRS, 2), colony.genes[0][3], dtype=np.float32)
    ants = []
    for i in range(NUM_ANTS):
        ant = Ant(colony.genes[0], ENABLE_CURIOSITY)
        ants.append(ant)
    return ants, trail, editable_map

def reset_all():
    batch_ct = 1
    batch = []
    for i in range(BATCH_SIZE):
        new_colony = Colony(i)
        batch.append(new_colony)
        new_colony.print_info()
    tournament = []
    winners = []
    batch_avg = []
    batch_min = []
    colonies_tested = 0
    colony = batch[0]
    return batch_ct, batch, tournament, winners, batch_avg, batch_min, colonies_tested, colony


# In[7]:


# SETUP
display = pygame.display.set_mode([RES+CELL_PIX/2, RES+TOP])

WHITE_ROW = np.full((CELLS, 1), 255)
BLACK_ROW = np.zeros((CELLS, 1))

#global fps_cap
FPS_MAX = 1000
fps_cap = FPS_MAX

manager = pygame_gui.UIManager([RES+CELL_PIX/2, RES+TOP])
fps_text = pygame_gui.elements.UILabel(text="FPS",relative_rect=pygame.Rect((0, 5), (200, 40)), manager=manager)
slow_button = pygame_gui.elements.UIButton(relative_rect=pygame.Rect((200, 5), (90, 40)), text='Slow', manager=manager)
fast_button = pygame_gui.elements.UIButton(relative_rect=pygame.Rect((300, 5), (90, 40)), text='Fast', manager=manager)
ff_button = pygame_gui.elements.UIButton(relative_rect=pygame.Rect((400, 5), (90, 40)), text='FFast', manager=manager)
col_text = pygame_gui.elements.UILabel(text="FOOD",relative_rect=pygame.Rect((500, 5), (300, 40)), manager=manager)



# MAIN LOOP
frame_steps = 300
fps_history = []
running = True

run_num = 0
batch_ct, batch, tournament, winners, batch_avg, batch_min, colonies_tested, colony = reset_all()
ants, trail, editable_map = reset_ants(colony, editable_map)
with open(filename, 'a', newline='') as file:
    wr = csv.writer(file, quoting=csv.QUOTE_ALL)
    wr.writerow(["New Run: ", run_num])
    wr.writerow(["Gen: ", "Avg", "Best","Genes"])
    print("New Run", run_num)

while running:
    # Check pygame events for input
    events = pygame.event.get()
    for event in events:
        if event.type == pygame.QUIT:
            running = False
        if event.type == pygame.USEREVENT:
            if event.user_type == pygame_gui.UI_BUTTON_PRESSED: # Handle buttons
                if event.ui_element == slow_button:
                    fps_cap = 30
                    frame_steps = 1
                if event.ui_element == fast_button:
                    fps_cap = FPS_MAX
                    frame_steps = 1
                if event.ui_element == ff_button:
                    fps_cap = FPS_MAX
                    frame_steps = 300
        manager.process_events(event)
    
    # Do time-steps
    for step in range(frame_steps):
        trail *= (1-colony.genes[0][0]/1000) # evaporate trail
        for ant in ants:
            dir_moved, colony.food, colony.stored_food = ant.move(trail, editable_map, colony.food, colony.stored_food)
        colony.elapsed += 1
        if colony.food >= FOOD_GOAL: break
    
    # Handle gui
    dt = clock.tick(fps_cap)/1000.0
    fps = frame_steps/dt
    fps_history.append(fps)
    if len(fps_history) > 10:
        avg_fps = sum(fps_history) / len(fps_history)
        fps_history = []
        fps_text.set_text( "Steps/s: {a:.1f}".format(a=avg_fps) )
    col_text.set_text( "Food: {a:.0f}, Time: {b:d}".format(a=colony.food, b=colony.elapsed) )
    
    # Handle colony checks
    done = False
    if colony.food >= FOOD_GOAL:
        done = True
    elif (colony.elapsed > TIME_LIMIT) or (colony.food < FOOD_GOAL/20 and colony.elapsed > TIME_LIMIT/2):
        if colony.food == 0:
            colony.elapsed =  float("inf")
        else:
            colony.elapsed = int(colony.elapsed * FOOD_GOAL / colony.food)
        done = True
    elif colony.stored_food >= 10000:
        colony.stored_food -= 10000
        ant = Ant(colony.genes[ACTIVE_CASTES-1], ENABLE_CURIOSITY)
        ants.append(ant)
        
    # End sim and go to next sim
    if done:
        colonies_tested += 1
        print("ID: ",colony.id_num, " Steps: ", colony.elapsed, "Food: ", colony.food)
        tournament.append(colony)
        # Determine winners
        if colonies_tested % TOURNAMENT_SIZE == 0:
            min_elapsed = float("inf")
            winner = None
            for rival in tournament:
                if rival.elapsed <= min_elapsed:
                    min_elapsed = rival.elapsed
                    winner = rival
            winners.append(winner)
            print("Winner: ", winner.id_num, " Time: ", winner.elapsed)
            tournament = []
            # If batch finished, get next batch
            if colonies_tested % BATCH_SIZE == 0:
                # Store stats
                with open(filename, 'a', newline='') as file:
                    wr = csv.writer(file, quoting=csv.QUOTE_ALL)
                    avg = sum([item.elapsed for item in batch]) / len(batch)
                    batch_avg.append(avg)
                    times = []
                    for item in batch: times.append(item.elapsed)
                    best = min(times)
                    batch_min.append(best)
                    best_idx = times.index(best)
                    bgenes = batch[best_idx].genes[0]
                    wr.writerow([batch_ct, avg, best, bgenes[0], bgenes[1], bgenes[2], bgenes[3], bgenes[4]]) 
                    print("Best:", best, "Avg:", avg)
                with open('batch.pkl', 'wb') as output:
                    pickle.dump(batch, output, -1)
                # Get next batch
                if batch_ct >= MAX_GEN:
                    run_num += 1
                    batch_ct, batch, tournament, winners, batch_avg, batch_min, colonies_tested, colony = reset_all()
                    ants, trail, editable_map = reset_ants(colony, editable_map)
                    with open(filename, 'a', newline='') as file:
                        wr = csv.writer(file, quoting=csv.QUOTE_ALL)
                        wr.writerow(["New Run: ", run_num])
                        wr.writerow(["Gen: ", "Avg", "Best","Genes"])
                        print("New Run", run_num)
                else:
                    batch_ct += 1
                    batch = []
                    for winner in winners:
                        batch.append(winner)
                    num_pairs = int(len(winners) / 2)
                    for i in range(num_pairs):
                        to_fill = int( (BATCH_SIZE - len(winners)) / num_pairs)
                        for j in range(to_fill):
                            child = Colony(colonies_tested + i*to_fill + j)
                            child.gene_recombination(winners[2*i], winners[2*i + 1])
                            child.mutate()
                            batch.append(child)
                    for member in batch:
                        member.print_info()
                        member.elapsed = 0
                        member.food = 0
                        member.stored_food = 0
                    random.shuffle(batch)
                    print("New Batch:", batch_ct, " Size: ", len(batch))
                    winners = []
        colony = batch[colonies_tested % BATCH_SIZE]
        ants, trail, editable_map = reset_ants(colony, editable_map)
    manager.update(dt)
        
    # Draw map
    display.fill(WHITE)
    color_mult = 255 / colony.genes[0][1] / 30 # Normalize color max to 30 max trail layers
    trail_food = np.clip(color_mult * np.sum(trail[:,:,:,0], axis=2), 0, 255) # Sum over directions and map a color value
    trail_home = np.clip(color_mult * np.sum(trail[:,:,:,1], axis=2), 0, 255) # Same for from_home trail
    for row in range(CELLS):
        # Trail surface
        food_trail_row = np.expand_dims(trail_food[:, row], axis=1)
        home_trail_row = np.expand_dims(trail_home[:, row], axis=1)
        trail_array = np.dstack([WHITE_ROW, 255-food_trail_row, 255-home_trail_row])
        map_row(display, trail_array, row, WHITE)
        # Map surface
        home_row = np.expand_dims(editable_map[:, row, 0], axis=1)
        map_array = np.dstack([255-home_row, 255-home_row, 255-home_row])
        map_row(display, map_array, row, WHITE)
        
        food_row = np.expand_dims(editable_map[:, row, 1], axis=1)
        map_array = np.dstack([BLACK_ROW, food_row, BLACK_ROW])
        map_row(display, map_array, row, BLACK)
        
        wall_row = np.expand_dims(editable_map[:, row, 2], axis=1)
        map_array = np.dstack([BLACK_ROW, BLACK_ROW, wall_row])
        map_row(display, map_array, row, BLACK)
    
    # Draw ants
    for ant in ants:
        rotated_ant = pygame.transform.rotate(antImg, 270-60*ant.rot)
        rotated_ant = pygame.transform.scale(rotated_ant, (CELL_PIX, CELL_PIX))
        offset = 0
        if ant.y % 2: offset = CELL_PIX/2
        display.blit(rotated_ant, (ant.x*CELL_PIX + offset, ant.y*CELL_PIX+TOP))

    manager.draw_ui(display)
    pygame.display.update()
pygame.quit()
